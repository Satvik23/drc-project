package com.drc.reportbuilder;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBatchProcessing
public class DrcApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrcApplication.class, args);
	}
}
