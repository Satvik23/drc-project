package com.drc.reportbuilder.batch.configuration;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.drc.reportbuilder.entities.RevenueMasterDjone;

@Configuration
@EnableBatchProcessing
public class DatabaseBatchConfiguration {

	@Autowired
	 private JobBuilderFactory jobBuilderFactory;
	 
	 @Autowired
	 private StepBuilderFactory stepBuilderFactory;
	 
	 @Autowired
	 private DataSource dataSource;
	 
	 @Autowired
	 private RevenueMasterDjoneExcelWriter revenueMasterDjoneExcelWriter;
	 
	/*
	 * @ConfigurationProperties(prefix = "datasource.postgres")
	 * 
	 * @Bean
	 * 
	 * @Primary public DataSource dataSource() { return DataSourceBuilder .create()
	 * .build(); }
	 */
	 
	/*
	 * @Bean
	 * 
	 * public DataSource DataBaseDataSource() {
	 * 
	 * DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
	 * dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
	 * dataSourceBuilder.url("dbc:postgresql://localhost:5432/postgres");
	 * dataSourceBuilder.username("postgres");
	 * dataSourceBuilder.password("postgres"); return dataSourceBuilder.build(); }
	 */
	 
	 @Bean(destroyMethod="")
	 public JdbcCursorItemReader<RevenueMasterDjone> databaseReader(@Value("#{jobParameters[query]}") String query){
	  JdbcCursorItemReader<RevenueMasterDjone> reader = new JdbcCursorItemReader<RevenueMasterDjone>();
	  reader.setDataSource(dataSource);
	  reader.setSql("SELECT id,fiscal_year_week FROM test.revenue_master_djone");
	  reader.setRowMapper(new RevenueMasterDjoneRowMapper());
	  return reader;
	 }
	 
	 @Bean
	 public RevenueMasterDjoneProcessor databaseProcessor(){
	  return new RevenueMasterDjoneProcessor();
	 }
	 
	/*
	 * @Bean public FlatFileItemWriter<RevenueMasterDjone> databaseWriter(){
	 * FlatFileItemWriter<RevenueMasterDjone> writer = new
	 * FlatFileItemWriter<RevenueMasterDjone>(); writer.setResource(new
	 * ClassPathResource("user.csv")); writer.setAppendAllowed(true);
	 * writer.setLineAggregator(new DelimitedLineAggregator<RevenueMasterDjone>() {{
	 * setDelimiter(","); setFieldExtractor(new
	 * BeanWrapperFieldExtractor<RevenueMasterDjone>() {{ setNames(new String[] {
	 * "id" }); }}); }}); writer.open(new ExecutionContext()); return writer; }
	 */
	 
	 
	 
	 @Bean
	 public Step step1() {
	  return stepBuilderFactory.get("step1").<RevenueMasterDjone, RevenueMasterDjone> chunk(100)
	    .reader(databaseReader(""))
	    .processor(databaseProcessor())
	    .writer(revenueMasterDjoneExcelWriter)
	    .build();
	 }
	 
	 @Bean("exportJob")
	 public Job exportUserJob() {
	  return jobBuilderFactory.get("exportUserJob")
	    .incrementer(new RunIdIncrementer())
	    .flow(step1())
	    .end()
	    .build();
	 }
	 
	/*
	 * @Bean public HibernatePagingItemReader<RevenueMasterDjone> databaseReader(){
	 * HibernatePagingItemReader<RevenueMasterDjone> reader = new
	 * HibernatePagingItemReader<RevenueMasterDjone>();
	 * reader.setName("revenueMasterDJOReader");
	 * reader.setSessionFactory(sessionFactory);
	 * reader.setQueryString("from RevenueMasterDjone r");
	 * reader.setSaveState(false);
	 * 
	 * try { reader.afterPropertiesSet(); } catch (Exception e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); }
	 * 
	 * 
	 * return reader; }
	 * 
	 * @Bean public RevenueMasterDjoneProcessor dataBaseProcessor(){ return new
	 * RevenueMasterDjoneProcessor(); }
	 * 
	 * @Bean public FlatFileItemWriter<RevenueMasterDjone> dataBaseWriter(){
	 * FlatFileItemWriter<RevenueMasterDjone> writer = new
	 * FlatFileItemWriter<RevenueMasterDjone>(); writer.setResource(new
	 * ClassPathResource("users.csv")); writer.setLineAggregator(new
	 * DelimitedLineAggregator<RevenueMasterDjone>() {{ setDelimiter(",");
	 * setFieldExtractor(new BeanWrapperFieldExtractor<RevenueMasterDjone>() {{
	 * setNames(new String[] { "id", "fiscalYearWeek" }); }}); }});
	 * 
	 * return writer; }
	 * 
	 * 
	 * 
	 * @Bean public Step step1() { return
	 * stepBuilderFactory.get("step1").<RevenueMasterDjone, RevenueMasterDjone>
	 * chunk(10) .reader(databaseReader()) .processor(dataBaseProcessor())
	 * .writer(dataBaseWriter()) .build(); }
	 * 
	 * @Bean public Job exportUserJob() { return
	 * jobBuilderFactory.get("exportUserJob") .incrementer(new RunIdIncrementer())
	 * .flow(step1()) .end() .build(); }
	 * 
	 * @SuppressWarnings("deprecation")
	 * 
	 * @Bean public HibernateJpaSessionFactoryBean sessionFactory() { return new
	 * HibernateJpaSessionFactoryBean(); }
	 */
}
