package com.drc.reportbuilder.batch.configuration;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.excel.poi.PoiItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.drc.reportbuilder.entities.RevenueMasterDjone;
import com.drc.reportbuilder.entities.RevenueMasterDjoneRepository;

@Configuration
@EnableTransactionManagement
public class BatchConfiguration{

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private PlatformTransactionManager platformTransactionManager;
	
	@Autowired
	private RevenueMasterDjoneRepository revenueMasterDjoneRepository;
	
	@Bean
	public ItemWriter<RevenueMasterDjone> writer() {
	    RepositoryItemWriter<RevenueMasterDjone> writer = new RepositoryItemWriter<>();
	    writer.setRepository(revenueMasterDjoneRepository);
	    writer.setMethodName("save");

	    try {
	        writer.afterPropertiesSet();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    return writer;
	}
	

	@Bean
	@StepScope
	ItemStreamReader<RevenueMasterDjone> reader(@Value("#{jobParameters[fullPathFileName]}") String pathToFile) {
		PoiItemReader reader = new PoiItemReader();
		reader.setResource(new FileSystemResource(pathToFile));
		reader.setRowMapper(new RowMapperImpl());
		try {
			reader.afterPropertiesSet();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reader.setLinesToSkip(1);
		return reader;
	}
	
	@Bean("importJob")
	public Job importFileJob(JobCompletionNotificationListener listener, Step step) {
		return jobBuilderFactory.get("importFileJob").incrementer(new RunIdIncrementer()).listener(listener).flow(step)
				.end().build();
	}

	@Bean
	public RevenueMasterDjoneProcessor processor() {
		return new RevenueMasterDjoneProcessor();
	}

	@Bean
	public Step step(ItemWriter<RevenueMasterDjone> writer) {
		return stepBuilderFactory.get("step").<RevenueMasterDjone, RevenueMasterDjone>chunk(1000).reader(reader("")).processor(processor())
				.writer(writer).transactionManager(platformTransactionManager).allowStartIfComplete(true).build();
	}
	// end::jobstep[]
}
