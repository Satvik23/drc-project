package com.drc.reportbuilder.batch.configuration;

import java.util.Date;

import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.mapping.BeanWrapperRowMapper;
import org.springframework.batch.item.excel.support.rowset.RowSet;

import com.drc.reportbuilder.entities.RevenueMasterDjone;

public class RowMapperImpl extends BeanWrapperRowMapper<RevenueMasterDjone> {
    public RowMapperImpl() {
    }
 
    @Override
    public RevenueMasterDjone mapRow(RowSet rs) {
        if (rs == null || rs.getCurrentRow() == null) {
            return null;
        }
        RevenueMasterDjone revenueMasterDjone = new RevenueMasterDjone(); // try to set at a time
        int i = 0;
        
        //revenueMasterDjone.setFiscalDate(new Date(rs.getColumnValue(i++)));
        revenueMasterDjone.setFiscalYearWeek(rs.getColumnValue(i++));
        revenueMasterDjone.setFiscalPeriod(rs.getColumnValue(i++));
        revenueMasterDjone.setWeekOfYear(rs.getColumnValue(i++));
        revenueMasterDjone.setFiscalYear(rs.getColumnValue(i++));
        //revenueMasterDjone.setRequestDate(new Date(rs.getColumnValue(i++)));
        //revenueMasterDjone.setOrderedDate(new Date(rs.getColumnValue(i++)));
        //revenueMasterDjone.setInvoiceDate(new Date(rs.getColumnValue(i++)));
        revenueMasterDjone.setOrderNumber(rs.getColumnValue(i++));
        revenueMasterDjone.setInvoiceNumber(rs.getColumnValue(i++));
        revenueMasterDjone.setOrderType(rs.getColumnValue(i++));
        revenueMasterDjone.setSalesOrganization(rs.getColumnValue(i++));
        revenueMasterDjone.setRegion(rs.getColumnValue(i++));
        revenueMasterDjone.setDpName(rs.getColumnValue(i++));
        revenueMasterDjone.setSalesRepName(rs.getColumnValue(i++));
        revenueMasterDjone.setProductFamily(rs.getColumnValue(i++));
        revenueMasterDjone.setProductBrand(rs.getColumnValue(i++));
        revenueMasterDjone.setProductModel(rs.getColumnValue(i++));
        revenueMasterDjone.setBillToCustomerNumber(rs.getColumnValue(i++));
        revenueMasterDjone.setBillToAccountName(rs.getColumnValue(i++));
        revenueMasterDjone.setBillToLocationStreetAddress(rs.getColumnValue(i++));
        revenueMasterDjone.setBillToLocationCity(rs.getColumnValue(i++));
        revenueMasterDjone.setBillToLocationState(rs.getColumnValue(i++));
        revenueMasterDjone.setBillToLocationPostalCode(rs.getColumnValue(i++));
        revenueMasterDjone.setBillToSiteCategoryCode(rs.getColumnValue(i++));
        revenueMasterDjone.setShipToCustomerNumber(rs.getColumnValue(i++));
        revenueMasterDjone.setShipToCustomerAccountName(rs.getColumnValue(i++));
        revenueMasterDjone.setShipToLocationStreetAddress(rs.getColumnValue(i++));
        revenueMasterDjone.setShipToLocationCity(rs.getColumnValue(i++));
        revenueMasterDjone.setShipToLocationState(rs.getColumnValue(i++));
        revenueMasterDjone.setShipToLocationPostalCode(rs.getColumnValue(i++));
        revenueMasterDjone.setShipToSiteCategoryCode(rs.getColumnValue(i++));
        revenueMasterDjone.setEndCustomerName(rs.getColumnValue(i++));
        revenueMasterDjone.setEndCustomerAddress(rs.getColumnValue(i++));
        revenueMasterDjone.setEndCustomerCity(rs.getColumnValue(i++));
        revenueMasterDjone.setEndCustomerState(rs.getColumnValue(i++));
        revenueMasterDjone.setEndCustomerShortZip(rs.getColumnValue(i++));
        revenueMasterDjone.setTotalActualSalesAmount(rs.getColumnValue(i++));
        revenueMasterDjone.setTotalNetQty(rs.getColumnValue(i++));
        revenueMasterDjone.setTotalNetSalesAmt(rs.getColumnValue(i++));
        revenueMasterDjone.setTotalCommissionSalesAmt(rs.getColumnValue(i++));
        return revenueMasterDjone;
    }
 
}