package com.drc.reportbuilder.batch.configuration;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.drc.reportbuilder.entities.RevenueMasterDjone;
import com.drc.reportbuilder.entities.RevenueMasterDjoneRepository;

public class RevenueMasterDjoneWriter implements ItemWriter<RevenueMasterDjone> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RevenueMasterDjoneWriter.class);

	@Autowired
	private RevenueMasterDjoneRepository revenueMasterDjoneRepository;
	
    @Override
    public void write(List<? extends RevenueMasterDjone> items) throws Exception {
        LOGGER.info("Received the information of {} RevenueMasterDjone", items.size());
        RepositoryItemWriter<RevenueMasterDjone> writer = new RepositoryItemWriter<>();
	    writer.setRepository(revenueMasterDjoneRepository);
	    writer.setMethodName("save");
        items.forEach(i -> LOGGER.debug("Received the information of a RevenueMasterDjone: {}", i));
    }

}
