package com.drc.reportbuilder.batch.configuration;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.drc.reportbuilder.entities.RevenueMasterDjone;

public class RevenueMasterDjoneRowMapper implements RowMapper<RevenueMasterDjone>{

	  @Override
	  public RevenueMasterDjone mapRow(ResultSet rs, int rowNum) throws SQLException {
		  RevenueMasterDjone user = new RevenueMasterDjone();
	   user.setId(rs.getInt("id"));
	   user.setFiscalYear(rs.getString("fiscal_year_week"));
	   
	   return user;
	  }

}
