package com.drc.reportbuilder.batch.configuration;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.drc.reportbuilder.entities.RevenueMasterDjone;

public class RevenueMasterDjoneProcessor implements ItemProcessor<RevenueMasterDjone, RevenueMasterDjone> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RevenueMasterDjoneProcessor.class);

    @Override
    public RevenueMasterDjone process(RevenueMasterDjone item) throws Exception {
        LOGGER.info("Processing revenue master djone information: {}", item);
        item.setUploadOn(new Date()); // use jodas
        
        //revenueMasterDjone.setModifiedOn(new Date(rs.getColumnValue(1)));
        //revenueMasterDjone.setDeletedOn(new Date(rs.getColumnValue(2)));
        item.setIsActive("1");
        return item;
    }
}