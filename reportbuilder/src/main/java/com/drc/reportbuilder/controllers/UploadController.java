package com.drc.reportbuilder.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.drc.reportbuilder.batch.configuration.BatchConfiguration;
import com.drc.reportbuilder.entities.RevenueMasterDjoneRepository;

@Controller
public class UploadController {

	// Save the uploaded file to this folder
	private static String UPLOADED_FOLDER = "D:\\tmp\\";

	@Autowired
	private RevenueMasterDjoneRepository revenueMasterDjoneRepository;

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	@Qualifier("importJob")
	private Job revenueMasterDjoneImportJob;
	
	@Autowired
	@Qualifier("exportJob")
	private Job revenueMasterDjoneExportJob;

	@GetMapping("/")
	public String index() {
		return "upload";
	}
	
	@GetMapping("/download")
	public String download() {
		return "download";
	}

	@PostMapping("/upload")
	public String singleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "redirect:uploadStatus";
		}

		try {

			// Get the file and save it somewhere
			byte[] bytes = file.getBytes();
			Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
			Files.write(path, bytes);

			JobExecution jobExecution = jobLauncher.run(revenueMasterDjoneImportJob,
					new JobParametersBuilder().addString("fullPathFileName", path.toString())
							.addLong("time", System.currentTimeMillis()).toJobParameters());

			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + file.getOriginalFilename() + "'");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JobExecutionAlreadyRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobRestartException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobInstanceAlreadyCompleteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobParametersInvalidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "redirect:/uploadStatus";
	}
	
	@PostMapping("/download")
	public String singleFileDownload(@RequestParam("query") String query, RedirectAttributes redirectAttributes) {

		if (query.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please provide a query");
			return "redirect:uploadStatus";
		}

		try {
			JobExecution jobExecution = jobLauncher.run(revenueMasterDjoneExportJob,
					new JobParametersBuilder().addString("query", query)
							.addLong("time", System.currentTimeMillis()).toJobParameters());

			redirectAttributes.addFlashAttribute("message",
					"Data successfully downloaded '" + "" + "'");

		} catch (JobExecutionAlreadyRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobRestartException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobInstanceAlreadyCompleteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JobParametersInvalidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "redirect:/uploadStatus";
	}

	@GetMapping("/uploadStatus")
	public String uploadStatus() {
		return "uploadStatus";
	}
}
