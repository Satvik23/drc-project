package com.drc.reportbuilder.entities;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cms_avaya_drc_isr_ga_mnl_aux")


public class CmsAvayaDrcIsrGaMnlAux implements Serializable{
	private static final long serialVersionUID = -2343243243242432341L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
	@SequenceGenerator(name="id_generator", sequenceName = "cms_avaya_drc_isr_ga_mnl_aux_id_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "upload_on")
	private Date uploadOn;
	
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_active")
	private String isActive;
	@Column(name = "date")
	private String date;

	@Column(name = "month")
	private String month;

	@Column(name = "team_leader")
	private String team_leader;

	@Column(name = "agent_name")
	private String agent_name;

	@Column(name = "login_id")
	private Integer login_id;

	@Column(name = "staffed_time")
	private Integer staffed_time;

	@Column(name = "aux_time")
	private Integer aux_time;

	@Column(name = "time_in_0")
	private Integer time_in_0;

	@Column(name = "time_in_break")
	private Integer time_in_break;

	@Column(name = "time_in_lunch")
	private Integer time_in_lunch;

	@Column(name = "time_in_email")
	private Integer time_in_email;
	@Column(name = "time_in_outbound_call")
	private Integer time_in_outbound_call;
	@Column(name = "time_in_meeting")
	private Integer time_in_meeting;
	@Column(name = "time_in_training")
	private Integer time_in_training;
	@Column(name = "time_in_l2_technical_feedback")
	private Integer time_in_l2_technical_feedback;
	@Column(name = "time_in_bio_break")
	private Integer time_in_bio_break;
	@Column(name = "time_in_system_outage")
	private Integer time_in_system_outage;

	public String getAgent_name() {
		return agent_name;
	}
	 public Integer getAux_time() {
		return aux_time;
	}
	 public String getDate() {
		return date;
	}
	 public Date getDeletedOn() {
		return deletedOn;
	}
	 public long getId() {
		return id;
	}
	
	 public String getIsActive() {
		return isActive;
	}
	 public Integer getLogin_id() {
		return login_id;
	}
	 public Date getModifiedOn() {
		return modifiedOn;
	}
	 public String getMonth() {
		return month;
	}
	 public Integer getStaffed_time() {
		return staffed_time;
	}
	 public String getTeam_leader() {
		return team_leader;
	}
	 public Integer getTime_in_0() {
		return time_in_0;
	}
	 public Integer getTime_in_break() {
		return time_in_break;
	}public Integer getTime_in_bio_break() {
		return time_in_bio_break;
	}
	public Integer getTime_in_email() {
		return time_in_email;
	}
	public Integer getTime_in_l2_technical_feedback() {
		return time_in_l2_technical_feedback;
	}
	public Integer getTime_in_lunch() {
		return time_in_lunch;
	}
	public Integer getTime_in_meeting() {
		return time_in_meeting;
	}
	public Integer getTime_in_outbound_call() {
		return time_in_outbound_call;
	}
	 public Integer getTime_in_system_outage() {
		return time_in_system_outage;
	}
	 public Integer getTime_in_training() {
		return time_in_training;
	}
	 public Date getUploadOn() {
		return uploadOn;
	}
	 public static long getSerialversionuid() {
		return serialVersionUID;
	}
	 public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	 public void setAux_time(Integer aux_time) {
		this.aux_time = aux_time;
	}
	 public void setDate(String date) {
		this.date = date;
	}
	 public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	 public void setId(long id) {
		this.id = id;
	}
	 public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	 public void setLogin_id(Integer login_id) {
		this.login_id = login_id;
	}
	 public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	 public void setMonth(String month) {
		this.month = month;
	}
	 public void setStaffed_time(Integer staffed_time) {
		this.staffed_time = staffed_time;
	}
	 public void setTeam_leader(String team_leader) {
		this.team_leader = team_leader;
	}
	 public void setTime_in_0(Integer time_in_0) {
		this.time_in_0 = time_in_0;
	}
	 public void setTime_in_bio_break(Integer time_in_bio_break) {
		this.time_in_bio_break = time_in_bio_break;
	}
	 public void setTime_in_break(Integer time_in_break) {
		this.time_in_break = time_in_break;
	}
	 public void setTime_in_email(Integer time_in_email) {
		this.time_in_email = time_in_email;
	}
	 public void setTime_in_l2_technical_feedback(Integer time_in_l2_technical_feedback) {
		this.time_in_l2_technical_feedback = time_in_l2_technical_feedback;
	}
	 public void setTime_in_lunch(Integer time_in_lunch) {
		this.time_in_lunch = time_in_lunch;
	}
	 public void setTime_in_meeting(Integer time_in_meeting) {
		this.time_in_meeting = time_in_meeting;
	}
	 public void setTime_in_outbound_call(Integer time_in_outbound_call) {
		this.time_in_outbound_call = time_in_outbound_call;
	}
	 public void setTime_in_system_outage(Integer time_in_system_outage) {
		this.time_in_system_outage = time_in_system_outage;
	}
	 public void setTime_in_training(Integer time_in_training) {
		this.time_in_training = time_in_training;
	} public void setUploadOn(Date uploadOn) {
		this.uploadOn = uploadOn;
	}
	

}
