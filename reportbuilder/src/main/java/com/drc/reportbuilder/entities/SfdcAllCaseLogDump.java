package com.drc.reportbuilder.entities;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "sfdc_all_case_log_dump")


public class SfdcAllCaseLogDump implements Serializable {
	private static final long serialVersionUID = -2343243243242432341L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
	@SequenceGenerator(name="id_generator", sequenceName = "sfdc_all_case_log_dump_id_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "upload_on")
	private Date uploadOn;
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	@Column(name = "total_days_to_close")
	private String total_days_to_close;
	@Column(name = "send_survey_mail_once_in_30_days")
	private Integer send_survey_mail_once_in_30_days;
	
	@Column(name = "case_closed_date_time")
	private Date case_closed_date_time;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getUploadOn() {
		return uploadOn;
	}
	public void setUploadOn(Date uploadOn) {
		this.uploadOn = uploadOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	public String getTotal_days_to_close() {
		return total_days_to_close;
	}
	public void setTotal_days_to_close(String total_days_to_close) {
		this.total_days_to_close = total_days_to_close;
	}
	public Integer getSend_survey_mail_once_in_30_days() {
		return send_survey_mail_once_in_30_days;
	}
	public void setSend_survey_mail_once_in_30_days(Integer send_survey_mail_once_in_30_days) {
		this.send_survey_mail_once_in_30_days = send_survey_mail_once_in_30_days;
	}
	public Date getCase_closed_date_time() {
		return case_closed_date_time;
	}
	public void setCase_closed_date_time(Date case_closed_date_time) {
		this.case_closed_date_time = case_closed_date_time;
	}
	public String getTotal_time_to_close() {
		return total_time_to_close;
	}
	public void setTotal_time_to_close(String total_time_to_close) {
		this.total_time_to_close = total_time_to_close;
	}
	public String getAging_time_close_hrs() {
		return aging_time_close_hrs;
	}
	public void setAging_time_close_hrs(String aging_time_close_hrs) {
		this.aging_time_close_hrs = aging_time_close_hrs;
	}
	public String getTotal_days_in_progress() {
		return total_days_in_progress;
	}
	public void setTotal_days_in_progress(String total_days_in_progress) {
		this.total_days_in_progress = total_days_in_progress;
	}
	public String getTotal_time_in_progress() {
		return total_time_in_progress;
	}
	public void setTotal_time_in_progress(String total_time_in_progress) {
		this.total_time_in_progress = total_time_in_progress;
	}
	public String getDjo_case_master_record_type() {
		return djo_case_master_record_type;
	}
	public void setDjo_case_master_record_type(String djo_case_master_record_type) {
		this.djo_case_master_record_type = djo_case_master_record_type;
	}
	public Date getDjo_case_master_last_modified_date() {
		return djo_case_master_last_modified_date;
	}
	public void setDjo_case_master_last_modified_date(Date djo_case_master_last_modified_date) {
		this.djo_case_master_last_modified_date = djo_case_master_last_modified_date;
	}
	public String getDjo_case_master_last_modified_by() {
		return djo_case_master_last_modified_by;
	}
	public void setDjo_case_master_last_modified_by(String djo_case_master_last_modified_by) {
		this.djo_case_master_last_modified_by = djo_case_master_last_modified_by;
	}
	public String getDjo_case_master_created_by() {
		return djo_case_master_created_by;
	}
	public void setDjo_case_master_created_by(String djo_case_master_created_by) {
		this.djo_case_master_created_by = djo_case_master_created_by;
	}
	public String getCustomer_communication_log() {
		return customer_communication_log;
	}
	public void setCustomer_communication_log(String customer_communication_log) {
		this.customer_communication_log = customer_communication_log;
	}
	public String getCustomer_communication() {
		return customer_communication;
	}
	public void setCustomer_communication(String customer_communication) {
		this.customer_communication = customer_communication;
	}
	public String getCommendation_comments() {
		return commendation_comments;
	}
	public void setCommendation_comments(String commendation_comments) {
		this.commendation_comments = commendation_comments;
	}
	public String getCommendation_recipient() {
		return commendation_recipient;
	}
	public void setCommendation_recipient(String commendation_recipient) {
		this.commendation_recipient = commendation_recipient;
	}
	public Integer getCommendation() {
		return commendation;
	}
	public void setCommendation(Integer commendation) {
		this.commendation = commendation;
	}
	public String getInvestigation_results() {
		return investigation_results;
	}
	public void setInvestigation_results(String investigation_results) {
		this.investigation_results = investigation_results;
	}
	public String getReason_for_agent_issue() {
		return reason_for_agent_issue;
	}
	public void setReason_for_agent_issue(String reason_for_agent_issue) {
		this.reason_for_agent_issue = reason_for_agent_issue;
	}
	public String getReason_for_delay() {
		return reason_for_delay;
	}
	public void setReason_for_delay(String reason_for_delay) {
		this.reason_for_delay = reason_for_delay;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getEscalation_comments() {
		return escalation_comments;
	}
	public void setEscalation_comments(String escalation_comments) {
		this.escalation_comments = escalation_comments;
	}
	public String getEscalation_category() {
		return escalation_category;
	}
	public void setEscalation_category(String escalation_category) {
		this.escalation_category = escalation_category;
	}
	public String getEscalation_root_cause() {
		return escalation_root_cause;
	}
	public void setEscalation_root_cause(String escalation_root_cause) {
		this.escalation_root_cause = escalation_root_cause;
	}
	public Integer getEscalation() {
		return escalation;
	}
	public void setEscalation(Integer escalation) {
		this.escalation = escalation;
	}
	public Integer getWithin_policy() {
		return within_policy;
	}
	public void setWithin_policy(Integer within_policy) {
		this.within_policy = within_policy;
	}
	public Integer getCall_back_required() {
		return call_back_required;
	}
	public void setCall_back_required(Integer call_back_required) {
		this.call_back_required = call_back_required;
	}
	public String getOwner_department() {
		return owner_department;
	}
	public void setOwner_department(String owner_department) {
		this.owner_department = owner_department;
	}
	public String getDjo_case_master_owner_name() {
		return djo_case_master_owner_name;
	}
	public void setDjo_case_master_owner_name(String djo_case_master_owner_name) {
		this.djo_case_master_owner_name = djo_case_master_owner_name;
	}
	public String getContacts_to_resolve() {
		return contacts_to_resolve;
	}
	public void setContacts_to_resolve(String contacts_to_resolve) {
		this.contacts_to_resolve = contacts_to_resolve;
	}
	public String getCase_details() {
		return case_details;
	}
	public void setCase_details(String case_details) {
		this.case_details = case_details;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public String getDepartment_owner() {
		return department_owner;
	}
	public void setDepartment_owner(String department_owner) {
		this.department_owner = department_owner;
	}
	public String getRoot_cause() {
		return root_cause;
	}
	public void setRoot_cause(String root_cause) {
		this.root_cause = root_cause;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getOrder1() {
		return order1;
	}
	public void setOrder1(String order1) {
		this.order1 = order1;
	}
	public Integer getKey_account() {
		return key_account;
	}
	public void setKey_account(Integer key_account) {
		this.key_account = key_account;
	}
	public String getAccount_name() {
		return account_name;
	}
	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}
	public String getAccount_number() {
		return account_number;
	}
	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}
	public String getClosed_loop() {
		return closed_loop;
	}
	public void setClosed_loop(String closed_loop) {
		this.closed_loop = closed_loop;
	}
	public String getFeedback_details() {
		return feedback_details;
	}
	public void setFeedback_details(String feedback_details) {
		this.feedback_details = feedback_details;
	}
	public Date getDjo_case_master_created_date() {
		return djo_case_master_created_date;
	}
	public void setDjo_case_master_created_date(Date djo_case_master_created_date) {
		this.djo_case_master_created_date = djo_case_master_created_date;
	}
	public Date getCase_created_date_time() {
		return case_created_date_time;
	}
	public void setCase_created_date_time(Date case_created_date_time) {
		this.case_created_date_time = case_created_date_time;
	}
	public Integer getSend_customer_feedback_survey_mailer() {
		return send_customer_feedback_survey_mailer;
	}
	public void setSend_customer_feedback_survey_mailer(Integer send_customer_feedback_survey_mailer) {
		this.send_customer_feedback_survey_mailer = send_customer_feedback_survey_mailer;
	}
	public Integer getCustomer_feedback_mailer_received() {
		return customer_feedback_mailer_received;
	}
	public void setCustomer_feedback_mailer_received(Integer customer_feedback_mailer_received) {
		this.customer_feedback_mailer_received = customer_feedback_mailer_received;
	}
	public String getCustomer_feedback() {
		return customer_feedback;
	}
	public void setCustomer_feedback(String customer_feedback) {
		this.customer_feedback = customer_feedback;
	}
	public String getCustomer_experience() {
		return customer_experience;
	}
	public void setCustomer_experience(String customer_experience) {
		this.customer_experience = customer_experience;
	}
	public Integer getRecommended_score() {
		return recommended_score;
	}
	public void setRecommended_score(Integer recommended_score) {
		this.recommended_score = recommended_score;
	}
	public Integer getRecommendation_score_converted() {
		return recommendation_score_converted;
	}
	public void setRecommendation_score_converted(Integer recommendation_score_converted) {
		this.recommendation_score_converted = recommendation_score_converted;
	}
	public Integer getRecommendation_score() {
		return recommendation_score;
	}
	public void setRecommendation_score(Integer recommendation_score) {
		this.recommendation_score = recommendation_score;
	}
	public Integer getDjo_satisfactory_score_converted() {
		return djo_satisfactory_score_converted;
	}
	public void setDjo_satisfactory_score_converted(Integer djo_satisfactory_score_converted) {
		this.djo_satisfactory_score_converted = djo_satisfactory_score_converted;
	}
	public Integer getDjo_satisfactory_core() {
		return djo_satisfactory_core;
	}
	public void setDjo_satisfactory_core(Integer djo_satisfactory_core) {
		this.djo_satisfactory_core = djo_satisfactory_core;
	}
	public Integer getDjo_satisfaction_core() {
		return djo_satisfaction_core;
	}
	public void setDjo_satisfaction_core(Integer djo_satisfaction_core) {
		this.djo_satisfaction_core = djo_satisfaction_core;
	}
	public Integer getCase_resolution_satisfactory_rating() {
		return case_resolution_satisfactory_rating;
	}
	public void setCase_resolution_satisfactory_rating(Integer case_resolution_satisfactory_rating) {
		this.case_resolution_satisfactory_rating = case_resolution_satisfactory_rating;
	}
	public Integer getCase_resolution_satisfactory_score() {
		return case_resolution_satisfactory_score;
	}
	public void setCase_resolution_satisfactory_score(Integer case_resolution_satisfactory_score) {
		this.case_resolution_satisfactory_score = case_resolution_satisfactory_score;
	}
	public Integer getCustomer_survey_rating() {
		return customer_survey_rating;
	}
	public void setCustomer_survey_rating(Integer customer_survey_rating) {
		this.customer_survey_rating = customer_survey_rating;
	}
	public Integer getCustomer_survey_score() {
		return customer_survey_score;
	}
	public void setCustomer_survey_score(Integer customer_survey_score) {
		this.customer_survey_score = customer_survey_score;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getOriginal_owner_department() {
		return original_owner_department;
	}
	public void setOriginal_owner_department(String original_owner_department) {
		this.original_owner_department = original_owner_department;
	}
	public String getOriginal_owner2() {
		return original_owner2;
	}
	public void setOriginal_owner2(String original_owner2) {
		this.original_owner2 = original_owner2;
	}
	public String getOriginal_owner() {
		return original_owner;
	}
	public void setOriginal_owner(String original_owner) {
		this.original_owner = original_owner;
	}
	public String getBusiness_unit() {
		return business_unit;
	}
	public void setBusiness_unit(String business_unit) {
		this.business_unit = business_unit;
	}
	public String getContact_type2() {
		return contact_type2;
	}
	public void setContact_type2(String contact_type2) {
		this.contact_type2 = contact_type2;
	}
	public String getContact_type() {
		return contact_type;
	}
	public void setContact_type(String contact_type) {
		this.contact_type = contact_type;
	}
	public String getOriginal_category2() {
		return original_category2;
	}
	public void setOriginal_category2(String original_category2) {
		this.original_category2 = original_category2;
	}
	public String getOriginal_category() {
		return original_category;
	}
	public void setOriginal_category(String original_category) {
		this.original_category = original_category;
	}
	public String getCase_number() {
		return case_number;
	}
	public void setCase_number(String case_number) {
		this.case_number = case_number;
	}
	public String getCase_contact_email() {
		return case_contact_email;
	}
	public void setCase_contact_email(String case_contact_email) {
		this.case_contact_email = case_contact_email;
	}
	public String getCase_contact_name() {
		return case_contact_name;
	}
	public void setCase_contact_name(String case_contact_name) {
		this.case_contact_name = case_contact_name;
	}
	public String getCase_status() {
		return case_status;
	}
	public void setCase_status(String case_status) {
		this.case_status = case_status;
	}
	public String getCase_type() {
		return case_type;
	}
	public void setCase_type(String case_type) {
		this.case_type = case_type;
	}
	public String getDjo_case_master_djo_case_number() {
		return djo_case_master_djo_case_number;
	}
	public void setDjo_case_master_djo_case_number(String djo_case_master_djo_case_number) {
		this.djo_case_master_djo_case_number = djo_case_master_djo_case_number;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Column(name = "total_time_to_close")
	private String total_time_to_close;
	@Column(name = "aging_time_close_hrs")
	private String aging_time_close_hrs;
	@Column(name = "total_days_in_progress")
	private String total_days_in_progress;
	@Column(name = "total_time_in_progress")
	private String total_time_in_progress;
	@Column(name = "djo_case_master_record_type")
	private String djo_case_master_record_type;
	@Column(name = "djo_case_master_last_modified_date")
	private Date djo_case_master_last_modified_date;
	@Column(name = "djo_case_master_last_modified_by")
	private String djo_case_master_last_modified_by;
	
	@Column(name = "djo_case_master_created_by")
	private String djo_case_master_created_by;
	@Column(name = "customer_communication_log")
	private String customer_communication_log;
	@Column(name = "customer_communication")
	private String customer_communication;
	@Column(name = "commendation_comments")
	private String commendation_comments;
	@Column(name = "commendation_recipient")
	private String commendation_recipient;
	@Column(name = "commendation")
	private Integer commendation;
	@Column(name = "investigation_results")
	private String investigation_results;
	@Column(name = "reason_for_agent_issue")
	private String reason_for_agent_issue;

	@Column(name = "reason_for_delay")
	private String reason_for_delay;
	@Column(name = "site")
	private String site;
	@Column(name = "escalation_comments")
	private String escalation_comments;
	@Column(name = "escalation_category")
	private String escalation_category;
	@Column(name = "escalation_root_cause")
	private String escalation_root_cause;
	@Column(name = "escalation")
	private Integer escalation;
	@Column(name = "within_policy")
	private Integer within_policy;
	@Column(name = "call_back_required")
	private Integer call_back_required;
	@Column(name = "owner_department")
	private String owner_department;
	@Column(name = "djo_case_master_owner_name")
	private String djo_case_master_owner_name;
	@Column(name = "contacts_to_resolve")
	private String contacts_to_resolve;
	@Column(name = "case_details")
	private String case_details;
	@Column(name = "resolution")
	private String resolution;
	@Column(name = "department_owner")
	private String department_owner;
	@Column(name = "root_cause")
	private String root_cause;
	@Column(name = "reason")
	private String reason;
	@Column(name = "category")
	private String category;
	@Column(name = "order1")
	private String order1;
	@Column(name = "key_account")
	private Integer key_account;
	@Column(name = "account_name")
	private String account_name;
	@Column(name = "account_number")
	private String account_number;
	@Column(name = "closed_loop")
	private String closed_loop;
	@Column(name = "feedback_details")
	private String feedback_details;
	@Column(name = "djo_case_master_created_date")
	private Date djo_case_master_created_date;
	@Column(name = "case_created_date_time")
	private Date case_created_date_time;
	@Column(name = "send_customer_feedback_survey_mailer")
	private Integer send_customer_feedback_survey_mailer;
	@Column(name = "customer_feedback_mailer_received")
	private Integer customer_feedback_mailer_received;
	@Column(name = "customer_feedback")
	private String customer_feedback;
	@Column(name = "customer_experience")
	private String customer_experience;
	@Column(name = "recommended_score")
	private Integer recommended_score;
	@Column(name = "recommendation_score_converted")
	private Integer recommendation_score_converted;
	@Column(name = "recommendation_score")
	private Integer recommendation_score;
	@Column(name = "djo_satisfactory_score_converted")
	private Integer djo_satisfactory_score_converted;
	@Column(name = "djo_satisfactory_core")
	private Integer djo_satisfactory_core;
	@Column(name = "djo_satisfaction_core")
	private Integer djo_satisfaction_core;
	@Column(name = "case_resolution_satisfactory_rating")
	private Integer case_resolution_satisfactory_rating;
	@Column(name = "case_resolution_satisfactory_score")
	private Integer case_resolution_satisfactory_score;
	@Column(name = "customer_survey_rating")
	private Integer customer_survey_rating;
	@Column(name = "customer_survey_score")
	private Integer customer_survey_score;
	@Column(name = "is_active")
	private String isActive;
	@Column(name = "original_owner_department")
	private String original_owner_department;
	@Column(name = "original_owner2")
	private String original_owner2;
	@Column(name = "original_owner")
	private String original_owner;
	@Column(name = "business_unit")
	private String business_unit;
	@Column(name = "contact_type2")
	private String contact_type2;
	@Column(name = "contact_type")
	private String contact_type;
	@Column(name = "original_category2")
	private String original_category2;
	@Column(name = "original_category")
	private String original_category;
	@Column(name = "case_number")
	private String case_number;
	@Column(name = "case_contact_email")
	private String case_contact_email;
	@Column(name = "case_contact_name")
	private String case_contact_name;
	@Column(name = "case_status")
	private String case_status;
	@Column(name = "case_type")
	private String case_type;
	@Column(name = "djo_case_master_djo_case_number")
	private String djo_case_master_djo_case_number;
	
	

}
