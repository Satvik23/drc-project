package com.drc.reportbuilder.entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RevenueMasterDjoneRepository extends JpaRepository<RevenueMasterDjone, Long> {
}
