package com.drc.reportbuilder.entities;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "oracle_obiee_acc_info_and_terms")

public class OracleObieeAccInfoAndTerms implements Serializable{
private static final long serialVersionUID = -2343243243242432341L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
	@SequenceGenerator(name="id_generator", sequenceName = "oracle_obiee_acc_info_and_terms_id_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "upload_on")
	private Date uploadOn;
	
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_active")
	private String isActive;
	
	@Column(name = "invoiced_date")
	private Date invoiced_date;
	@Column(name = "operating_unit_name")
	private String operating_unit_name;
	@Column(name = "collector")
	private String collector;
	@Column(name = "customer_number")
	private String customer_number;
	@Column(name = "customer")
	private String customer;
	@Column(name = "payment_terms")
	private String payment_terms;
	@Column(name = "overall_credit_limit")
	private String overall_credit_limit;
public String getCollector() {
	return collector;
}
public String getCustomer() {
	return customer;
}
public String getCustomer_number() {
	return customer_number;
}
public Date getDeletedOn() {
	return deletedOn;
}
public long getId() {
	return id;
}
public Date getInvoiced_date() {
	return invoiced_date;
}
public String getIsActive() {
	return isActive;
}
public Date getModifiedOn() {
	return modifiedOn;
}
public String getOperating_unit_name() {
	return operating_unit_name;
}
public String getOverall_credit_limit() {
	return overall_credit_limit;
}
public String getPayment_terms() {
	return payment_terms;
}
public Date getUploadOn() {
	return uploadOn;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}
public void setCollector(String collector) {
	this.collector = collector;
}
public void setCustomer(String customer) {
	this.customer = customer;
}
public void setCustomer_number(String customer_number) {
	this.customer_number = customer_number;
}
public void setDeletedOn(Date deletedOn) {
	this.deletedOn = deletedOn;
}
public void setId(long id) {
	this.id = id;
}
public void setInvoiced_date(Date invoiced_date) {
	this.invoiced_date = invoiced_date;
}
public void setIsActive(String isActive) {
	this.isActive = isActive;
}
public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
}
public void setOperating_unit_name(String operating_unit_name) {
	this.operating_unit_name = operating_unit_name;
} 
public void setOverall_credit_limit(String overall_credit_limit) {
	this.overall_credit_limit = overall_credit_limit;
}
public void setPayment_terms(String payment_terms) {
	this.payment_terms = payment_terms;
}
public void setUploadOn(Date uploadOn) {
	this.uploadOn = uploadOn;
}

	

}
