
package com.drc.reportbuilder.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "oracle_callnotes")

public class OracleCallNotes implements Serializable  {
	

		private static final long serialVersionUID = -2343243243242432341L;
		
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
		@SequenceGenerator(name="id_generator", sequenceName = "oracle_callnotes_id_seq", allocationSize=50)
		@Column(name = "id", updatable = false, nullable = false)
		private long id;
		
		@Column(name = "upload_on")
		private Date uploadOn;
		
		@Column(name = "modified_on")
		private Date modifiedOn;
		
		@Column(name = "deleted_on")
		private Date deletedOn;
		
		@Column(name = "is_active")
		private String isActive;
		
		@Column(name = "creator_name")
		private String creator_name;
		
		@Column(name = "customer_name")
		private String customer_name;
		
		@Column(name = "customer_number")
		private String customer_number;
		
		@Column(name = "note")
		private String note;
		
		@Column(name = "note_date")
		private Date note_date;
		
		@Column(name = "notes_detail")
		private String notes_detail;
		
		@Column(name = "Operating_unit")
		private String Operating_unit;
		public String getCustomer_name() {
			return customer_name;
		}
		public String getCreator_name() {
			return creator_name;
		}
		public String getCustomer_number() {
			return customer_number;
		}
		public Date getDeletedOn() {
			return deletedOn;
		}
		public long getId() {
			return id;
		}
		public String getIsActive() {
			return isActive;
		}
		public Date getModifiedOn() {
			return modifiedOn;
		}
		public String getNote() {
			return note;
		}
		public Date getNote_date() {
			return note_date;
		}
		public String getNotes_detail() {
			return notes_detail;
		}
		public String getOperating_unit() {
			return Operating_unit;
		}
		public Date getUploadOn() {
			return uploadOn;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		public void setCreator_name(String creator_name) {
			this.creator_name = creator_name;
		}
		public void setCustomer_name(String customer_name) {
			this.customer_name = customer_name;
		}
		public void setCustomer_number(String customer_number) {
			this.customer_number = customer_number;
		}
		public void setDeletedOn(Date deletedOn) {
			this.deletedOn = deletedOn;
		}
		public void setId(long id) {
			this.id = id;
		}
		public void setIsActive(String isActive) {
			this.isActive = isActive;
		}
		public void setModifiedOn(Date modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
		public void setNote(String note) {
			this.note = note;
		}
		public void setNote_date(Date note_date) {
			this.note_date = note_date;
		}
		public void setNotes_detail(String notes_detail) {
			this.notes_detail = notes_detail;
		}
		public void setOperating_unit(String operating_unit) {
			Operating_unit = operating_unit;
		}
		public void setUploadOn(Date uploadOn) {
			this.uploadOn = uploadOn;
		}
		
			}


