package com.drc.reportbuilder.entities;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "fdc_drc_ce_dump")


public class FdcDrcCeDump implements Serializable {
	private static final long serialVersionUID = -2343243243242432341L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
	@SequenceGenerator(name="id_generator", sequenceName = "fdc_drc_ce_dump_id_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "working_1")
	private String working_1;
	@Column(name = "working_2")
	private String working_2;
	@Column(name = "djo_case_master_djo_case_number")
	private String djo_case_master_djo_case_number;
	@Column(name = "djo_case_master_record_type")
	private String djo_case_master_record_type;
	@Column(name = "customer_survey_score")
	private String customer_survey_score;
	@Column(name = "case_resolution_satisfactory_score")
	private String case_resolution_satisfactory_score;
	@Column(name = "djo_satisfaction_score")
	private String djo_satisfaction_score;
	@Column(name = "recommendation_score")
	private String recommendation_score;
	@Column(name = "customer_experience")
	private String customer_experience;
	@Column(name = "case_created_date_time")
	private String case_created_date_time;
	@Column(name = "feedback_details")
	private String feedback_details;
	@Column(name = "closed_loop")
	private String closed_loop;
	@Column(name = "account_number")
	private String account_number;
	@Column(name = "account_name")
	private String account_name;
	@Column(name = "key_account")
	private String key_account;
	@Column(name = "order1")
	private String order1;
	@Column(name = "category")
	private String category;
	@Column(name = "reason")
	private String reason;
	


}
