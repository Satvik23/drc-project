package com.drc.reportbuilder.entities;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "oracle_obiee_usage_report")


public class OracleObieeUsageReport implements Serializable{
	private static final long serialVersionUID = -2343243243242432341L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
	@SequenceGenerator(name="id_generator", sequenceName = "oracle_obiee_usage_report_id_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "upload_on")
	private Date uploadOn;
	
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_active")
	private String isActive;
	@Column(name = "bill_to_account_name")
	private String bill_to_account_name;
	@Column(name = "invoiced_fiscal_week")
	private String invoiced_fiscal_week;
	@Column(name = "bill_to_location_street_address")
	private String bill_to_location_street_address;
	@Column(name = "bill_to_location_postal_code")
	private String bill_to_location_postal_code;
	@Column(name = "parent")
	private String parent;
	@Column(name = "business_unit")
	private String business_unit;
	@Column(name = "ship_to_customer_number")
	private String ship_to_customer_number;
	@Column(name = "ship_to_account_name")
	private String ship_to_account_name;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getUploadOn() {
		return uploadOn;
	}
	public void setUploadOn(Date uploadOn) {
		this.uploadOn = uploadOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getBill_to_account_name() {
		return bill_to_account_name;
	}
	public void setBill_to_account_name(String bill_to_account_name) {
		this.bill_to_account_name = bill_to_account_name;
	}
	public String getInvoiced_fiscal_week() {
		return invoiced_fiscal_week;
	}
	public void setInvoiced_fiscal_week(String invoiced_fiscal_week) {
		this.invoiced_fiscal_week = invoiced_fiscal_week;
	}
	public String getBill_to_location_street_address() {
		return bill_to_location_street_address;
	}
	public void setBill_to_location_street_address(String bill_to_location_street_address) {
		this.bill_to_location_street_address = bill_to_location_street_address;
	}
	public String getBill_to_location_postal_code() {
		return bill_to_location_postal_code;
	}
	public void setBill_to_location_postal_code(String bill_to_location_postal_code) {
		this.bill_to_location_postal_code = bill_to_location_postal_code;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getBusiness_unit() {
		return business_unit;
	}
	public void setBusiness_unit(String business_unit) {
		this.business_unit = business_unit;
	}
	public String getShip_to_customer_number() {
		return ship_to_customer_number;
	}
	public void setShip_to_customer_number(String ship_to_customer_number) {
		this.ship_to_customer_number = ship_to_customer_number;
	}
	public String getShip_to_account_name() {
		return ship_to_account_name;
	}
	public void setShip_to_account_name(String ship_to_account_name) {
		this.ship_to_account_name = ship_to_account_name;
	}
	public String getShip_to_location_street_address() {
		return ship_to_location_street_address;
	}
	public void setShip_to_location_street_address(String ship_to_location_street_address) {
		this.ship_to_location_street_address = ship_to_location_street_address;
	}
	public String getShip_to_location_city() {
		return ship_to_location_city;
	}
	public void setShip_to_location_city(String ship_to_location_city) {
		this.ship_to_location_city = ship_to_location_city;
	}
	public String getShip_to_location_state() {
		return ship_to_location_state;
	}
	public void setShip_to_location_state(String ship_to_location_state) {
		this.ship_to_location_state = ship_to_location_state;
	}
	public String getShip_to_location_postal_code() {
		return ship_to_location_postal_code;
	}
	public void setShip_to_location_postal_code(String ship_to_location_postal_code) {
		this.ship_to_location_postal_code = ship_to_location_postal_code;
	}
	public String getBill_to_customer_number() {
		return bill_to_customer_number;
	}
	public void setBill_to_customer_number(String bill_to_customer_number) {
		this.bill_to_customer_number = bill_to_customer_number;
	}
	public String getNet_sales_amt() {
		return net_sales_amt;
	}
	public void setNet_sales_amt(String net_sales_amt) {
		this.net_sales_amt = net_sales_amt;
	}
	public String getUnit_selling_price() {
		return unit_selling_price;
	}
	public void setUnit_selling_price(String unit_selling_price) {
		this.unit_selling_price = unit_selling_price;
	}
	public String getReturn_quantity() {
		return return_quantity;
	}
	public void setReturn_quantity(String return_quantity) {
		this.return_quantity = return_quantity;
	}
	public String getOrdered_quantity() {
		return ordered_quantity;
	}
	public void setOrdered_quantity(String ordered_quantity) {
		this.ordered_quantity = ordered_quantity;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getProduct_number() {
		return product_number;
	}
	public void setProduct_number(String product_number) {
		this.product_number = product_number;
	}
	public String getCorporate_brand() {
		return corporate_brand;
	}
	public void setCorporate_brand(String corporate_brand) {
		this.corporate_brand = corporate_brand;
	}
	public String getOrder_status() {
		return order_status;
	}
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
	public String getPo_number() {
		return po_number;
	}
	public void setPo_number(String po_number) {
		this.po_number = po_number;
	}
	public String getSales_order_number() {
		return sales_order_number;
	}
	public void setSales_order_number(String sales_order_number) {
		this.sales_order_number = sales_order_number;
	}
	public String getInvoice_number() {
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}
	public String getOrder_type() {
		return order_type;
	}
	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}
	public Date getShipped_date() {
		return shipped_date;
	}
	public void setShipped_date(Date shipped_date) {
		this.shipped_date = shipped_date;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Column(name = "ship_to_location_street_address")
	private String ship_to_location_street_address;
	@Column(name = "ship_to_location_city")
	private String ship_to_location_city;
	@Column(name = "ship_to_location_state")
	private String ship_to_location_state;
	@Column(name = "ship_to_location_postal_code")
	private String ship_to_location_postal_code;
	@Column(name = "bill_to_customer_number")
	private String bill_to_customer_number;
	@Column(name = "net_sales_amt")
	private String net_sales_amt;
	@Column(name = "unit_selling_price")
	private String unit_selling_price;
	@Column(name = "return_quantity")
	private String return_quantity;
	@Column(name = "ordered_quantity")
	private String ordered_quantity;
	@Column(name = "product_name")
	private String product_name;
	@Column(name = "product_number")
	private String product_number;
	@Column(name = "corporate_brand")
	private String corporate_brand;
	@Column(name = "order_status")
	private String order_status;
	@Column(name = "po_number")
	private String po_number;
	@Column(name = "sales_order_number")
	private String sales_order_number;
	@Column(name = "invoice_number")
	private String invoice_number;
	@Column(name = "order_type")
	private String order_type;
	@Column(name = "shipped_date")
	private Date shipped_date;
	@Column(name = "date")
	private Date date;
	

}
