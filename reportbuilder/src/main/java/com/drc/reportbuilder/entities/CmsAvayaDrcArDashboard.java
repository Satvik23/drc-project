package com.drc.reportbuilder.entities;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cms_avaya_drc_ar_dashboard")


public class CmsAvayaDrcArDashboard implements Serializable {
	private static final long serialVersionUID = -2343243243242432341L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
	@SequenceGenerator(name="id_generator", sequenceName = "cms_avaya_drc_ar_dashboard_id_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "upload_on")
	private Date uploadOn;
	
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_active")
	private String isActive;
	@Column(name = "date")
	private Date date;
	@Column(name = "split")
	private String split;
	@Column(name = "avaya_id")
	private Integer avaya_id;
	@Column(name = "acd_calls")
	private Integer acd_calls;
	@Column(name = "avg_acd_time")
	private String avg_acd_time;
	@Column(name = "avg_acw_time")
	private String avg_acw_time;
	@Column(name = "acd_time")
	private Integer acd_time;
	@Column(name = "acw_time")
	private Integer acw_time;
	@Column(name = "agent_ring_time")
	private Integer agent_ring_time;
	@Column(name = "other_time")
	private Integer other_time;
	@Column(name = "aux_time")
	private Integer aux_time;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getUploadOn() {
		return uploadOn;
	}
	public void setUploadOn(Date uploadOn) {
		this.uploadOn = uploadOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getSplit() {
		return split;
	}
	public void setSplit(String split) {
		this.split = split;
	}
	public Integer getAvaya_id() {
		return avaya_id;
	}
	public void setAvaya_id(Integer avaya_id) {
		this.avaya_id = avaya_id;
	}
	public Integer getAcd_calls() {
		return acd_calls;
	}
	public void setAcd_calls(Integer acd_calls) {
		this.acd_calls = acd_calls;
	}
	public String getAvg_acd_time() {
		return avg_acd_time;
	}
	public void setAvg_acd_time(String avg_acd_time) {
		this.avg_acd_time = avg_acd_time;
	}
	public String getAvg_acw_time() {
		return avg_acw_time;
	}
	public void setAvg_acw_time(String avg_acw_time) {
		this.avg_acw_time = avg_acw_time;
	}
	public Integer getAcd_time() {
		return acd_time;
	}
	public void setAcd_time(Integer acd_time) {
		this.acd_time = acd_time;
	}
	public Integer getAcw_time() {
		return acw_time;
	}
	public void setAcw_time(Integer acw_time) {
		this.acw_time = acw_time;
	}
	public Integer getAgent_ring_time() {
		return agent_ring_time;
	}
	public void setAgent_ring_time(Integer agent_ring_time) {
		this.agent_ring_time = agent_ring_time;
	}
	public Integer getOther_time() {
		return other_time;
	}
	public void setOther_time(Integer other_time) {
		this.other_time = other_time;
	}
	public Integer getAux_time() {
		return aux_time;
	}
	public void setAux_time(Integer aux_time) {
		this.aux_time = aux_time;
	}
	public Integer getAvail_time() {
		return avail_time;
	}
	public void setAvail_time(Integer avail_time) {
		this.avail_time = avail_time;
	}
	public Integer getStaffed_time() {
		return staffed_time;
	}
	public void setStaffed_time(Integer staffed_time) {
		this.staffed_time = staffed_time;
	}
	public Integer getAssists() {
		return assists;
	}
	public void setAssists(Integer assists) {
		this.assists = assists;
	}
	public Integer getTrans_out() {
		return trans_out;
	}
	public void setTrans_out(Integer trans_out) {
		this.trans_out = trans_out;
	}
	public Integer getHeld_calls() {
		return held_calls;
	}
	public void setHeld_calls(Integer held_calls) {
		this.held_calls = held_calls;
	}
	public Integer getAvg_hold_time() {
		return avg_hold_time;
	}
	public void setAvg_hold_time(Integer avg_hold_time) {
		this.avg_hold_time = avg_hold_time;
	}
	public Integer getAuxoutcalls() {
		return auxoutcalls;
	}
	public void setAuxoutcalls(Integer auxoutcalls) {
		this.auxoutcalls = auxoutcalls;
	}
	public Integer getAuxouttime() {
		return auxouttime;
	}
	public void setAuxouttime(Integer auxouttime) {
		this.auxouttime = auxouttime;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getSub_process() {
		return sub_process;
	}
	public void setSub_process(String sub_process) {
		this.sub_process = sub_process;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getTime1() {
		return time1;
	}
	public void setTime1(String time1) {
		this.time1 = time1;
	}
	public String getAdvisor() {
		return advisor;
	}
	public void setAdvisor(String advisor) {
		this.advisor = advisor;
	}
	public Integer getActual_ob_calls() {
		return actual_ob_calls;
	}
	public void setActual_ob_calls(Integer actual_ob_calls) {
		this.actual_ob_calls = actual_ob_calls;
	}
	public Integer getTotal_talk_time_ib() {
		return total_talk_time_ib;
	}
	public void setTotal_talk_time_ib(Integer total_talk_time_ib) {
		this.total_talk_time_ib = total_talk_time_ib;
	}
	public Integer getActual_ob_calls2() {
		return actual_ob_calls2;
	}
	public void setActual_ob_calls2(Integer actual_ob_calls2) {
		this.actual_ob_calls2 = actual_ob_calls2;
	}
	public Integer getAsa() {
		return asa;
	}
	public void setAsa(Integer asa) {
		this.asa = asa;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Column(name = "avail_time")
	private Integer avail_time;
	@Column(name = "staffed_time")
	private Integer staffed_time;
	@Column(name = "assists")
	private Integer assists;
	@Column(name = "trans_out")
	private Integer trans_out;
	@Column(name = "held_calls")
	private Integer held_calls;
	@Column(name = "avg_hold_time")
	private Integer avg_hold_time;
	@Column(name = "auxoutcalls")
	private Integer auxoutcalls;
	@Column(name = "auxouttime")
	private Integer auxouttime;
	@Column(name = "region")
	private String region;
	@Column(name = "process")
	private String process;
	@Column(name = "sub_process")
	private String sub_process;
	@Column(name = "manager")
	private String manager;
	@Column(name = "timezone")
	private String timezone;
	@Column(name = "time1")
	private String time1;
	@Column(name = "advisor")
	private String advisor;
	@Column(name = "actual_ob_calls")
	private Integer actual_ob_calls;
	@Column(name = "total_talk_time_ib")
	private Integer total_talk_time_ib;
	@Column(name = "actual_ob_calls2")
	private Integer actual_ob_calls2;
	@Column(name = "asa")
	private Integer asa;
	@Column(name = "week")
	private String week;
	@Column(name = "division")
	private String division;

	

}
