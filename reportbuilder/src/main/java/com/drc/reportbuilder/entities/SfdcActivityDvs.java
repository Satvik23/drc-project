package com.drc.reportbuilder.entities;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "sfdc_activity_dvs")


public class SfdcActivityDvs implements Serializable {
	private static final long serialVersionUID = -2343243243242432341L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
	@SequenceGenerator(name="id_generator", sequenceName = "sfdc_activity_dvs_id_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "upload_on")
	private Date uploadOn;
	
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_active")
	private String isActive;
	
	@Column(name = "account")
	private String account;
	@Column(name = "account_name")
	private String account_name;
	@Column(name = "claim_status")
	private String claim_status;
	@Column(name = "physician")
	private String physician;
	@Column(name = "patient")
	private String patient;
	@Column(name = "patient_mr")
	private String patient_mr;
	@Column(name = "claim_claim")
	private String claim_claim;
	@Column(name = "drc_b2b_order_placed")
	private String drc_b2b_order_placed;
	@Column(name = "claim_last_modified_date")
	private Date claim_last_modified_date;
	@Column(name = "claim_last_modified_by")
	private String claim_last_modified_by;
	@Column(name = "dos")
	private Date dos;
	@Column(name = "followup_date")
	private Date followup_date;
	@Column(name = "pcp_last_office_visit_date")
	private Date pcp_last_office_visit_date;
	@Column(name = "scp_sign_date")
	private Date scp_sign_date;
	@Column(name = "statement_of_certifying_physician")
	private String statement_of_certifying_physician;
	@Column(name = "physician_notes_on_qualifying_condition")
	private String physician_notes_on_qualifying_condition;
	@Column(name = "rx_therapeutic_shoes_inserts")
	private String rx_therapeutic_shoes_inserts;
	@Column(name = "documents_if_intial_in_person_fitting")
	private String documents_if_intial_in_person_fitting;
	@Column(name = "drc_shoe_fitting_form")
	private String drc_shoe_fitting_form;
	@Column(name = "doc_in_persons_fitting_time_of_dispense")
	private String doc_in_persons_fitting_time_of_dispense;
	@Column(name = "packing_slip_proof_of_delivery")
	private String packing_slip_proof_of_delivery;
	@Column(name = "aurthorization_of_payment_and_warranty")
	private String aurthorization_of_payment_and_warranty;
	@Column(name = "inside_pcs_rep")
	private String inside_pcs_rep;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getUploadOn() {
		return uploadOn;
	}
	public void setUploadOn(Date uploadOn) {
		this.uploadOn = uploadOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Date getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getAccount_name() {
		return account_name;
	}
	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}
	public String getClaim_status() {
		return claim_status;
	}
	public void setClaim_status(String claim_status) {
		this.claim_status = claim_status;
	}
	public String getPhysician() {
		return physician;
	}
	public void setPhysician(String physician) {
		this.physician = physician;
	}
	public String getPatient() {
		return patient;
	}
	public void setPatient(String patient) {
		this.patient = patient;
	}
	public String getPatient_mr() {
		return patient_mr;
	}
	public void setPatient_mr(String patient_mr) {
		this.patient_mr = patient_mr;
	}
	public String getClaim_claim() {
		return claim_claim;
	}
	public void setClaim_claim(String claim_claim) {
		this.claim_claim = claim_claim;
	}
	public String getDrc_b2b_order_placed() {
		return drc_b2b_order_placed;
	}
	public void setDrc_b2b_order_placed(String drc_b2b_order_placed) {
		this.drc_b2b_order_placed = drc_b2b_order_placed;
	}
	public Date getClaim_last_modified_date() {
		return claim_last_modified_date;
	}
	public void setClaim_last_modified_date(Date claim_last_modified_date) {
		this.claim_last_modified_date = claim_last_modified_date;
	}
	public String getClaim_last_modified_by() {
		return claim_last_modified_by;
	}
	public void setClaim_last_modified_by(String claim_last_modified_by) {
		this.claim_last_modified_by = claim_last_modified_by;
	}
	public Date getDos() {
		return dos;
	}
	public void setDos(Date dos) {
		this.dos = dos;
	}
	public Date getFollowup_date() {
		return followup_date;
	}
	public void setFollowup_date(Date followup_date) {
		this.followup_date = followup_date;
	}
	public Date getPcp_last_office_visit_date() {
		return pcp_last_office_visit_date;
	}
	public void setPcp_last_office_visit_date(Date pcp_last_office_visit_date) {
		this.pcp_last_office_visit_date = pcp_last_office_visit_date;
	}
	public Date getScp_sign_date() {
		return scp_sign_date;
	}
	public void setScp_sign_date(Date scp_sign_date) {
		this.scp_sign_date = scp_sign_date;
	}
	public String getStatement_of_certifying_physician() {
		return statement_of_certifying_physician;
	}
	public void setStatement_of_certifying_physician(String statement_of_certifying_physician) {
		this.statement_of_certifying_physician = statement_of_certifying_physician;
	}
	public String getPhysician_notes_on_qualifying_condition() {
		return physician_notes_on_qualifying_condition;
	}
	public void setPhysician_notes_on_qualifying_condition(String physician_notes_on_qualifying_condition) {
		this.physician_notes_on_qualifying_condition = physician_notes_on_qualifying_condition;
	}
	public String getRx_therapeutic_shoes_inserts() {
		return rx_therapeutic_shoes_inserts;
	}
	public void setRx_therapeutic_shoes_inserts(String rx_therapeutic_shoes_inserts) {
		this.rx_therapeutic_shoes_inserts = rx_therapeutic_shoes_inserts;
	}
	public String getDocuments_if_intial_in_person_fitting() {
		return documents_if_intial_in_person_fitting;
	}
	public void setDocuments_if_intial_in_person_fitting(String documents_if_intial_in_person_fitting) {
		this.documents_if_intial_in_person_fitting = documents_if_intial_in_person_fitting;
	}
	public String getDrc_shoe_fitting_form() {
		return drc_shoe_fitting_form;
	}
	public void setDrc_shoe_fitting_form(String drc_shoe_fitting_form) {
		this.drc_shoe_fitting_form = drc_shoe_fitting_form;
	}
	public String getDoc_in_persons_fitting_time_of_dispense() {
		return doc_in_persons_fitting_time_of_dispense;
	}
	public void setDoc_in_persons_fitting_time_of_dispense(String doc_in_persons_fitting_time_of_dispense) {
		this.doc_in_persons_fitting_time_of_dispense = doc_in_persons_fitting_time_of_dispense;
	}
	public String getPacking_slip_proof_of_delivery() {
		return packing_slip_proof_of_delivery;
	}
	public void setPacking_slip_proof_of_delivery(String packing_slip_proof_of_delivery) {
		this.packing_slip_proof_of_delivery = packing_slip_proof_of_delivery;
	}
	public String getAurthorization_of_payment_and_warranty() {
		return aurthorization_of_payment_and_warranty;
	}
	public void setAurthorization_of_payment_and_warranty(String aurthorization_of_payment_and_warranty) {
		this.aurthorization_of_payment_and_warranty = aurthorization_of_payment_and_warranty;
	}
	public String getInside_pcs_rep() {
		return inside_pcs_rep;
	}
	public void setInside_pcs_rep(String inside_pcs_rep) {
		this.inside_pcs_rep = inside_pcs_rep;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
