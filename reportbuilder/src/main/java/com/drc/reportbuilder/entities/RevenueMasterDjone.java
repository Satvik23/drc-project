package com.drc.reportbuilder.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "revenue_master_djone")
public class RevenueMasterDjone implements Serializable {

	private static final long serialVersionUID = -2343243243242432341L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
	@SequenceGenerator(name="id_generator", sequenceName = "revenue_master_djone_id_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "upload_on")
	private Date uploadOn;
	
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_active")
	private String isActive;
	
	@Column(name = "fiscal_date")
	private Date fiscalDate;
	
	@Column(name = "fiscal_year_week")
	private String fiscalYearWeek;
	
	@Column(name = "fiscal_period")
	private String fiscalPeriod;
	
	@Column(name = "week_of_year")
	private String weekOfYear;
	
	@Column(name = "fiscal_year")
	private String fiscalYear;
	
	@Column(name = "request_date")
	private Date requestDate;
	
	@Column(name = "ordered_date")
	private Date orderedDate;
	
	@Column(name = "invoice_date")
	private Date invoiceDate;
	
	@Column(name = "order_number")
	private String orderNumber;
	
	@Column(name = "invoice_number")
	private String invoiceNumber;
	
	@Column(name = "order_type")
	private String orderType;
	
	@Column(name = "sales_organization")
	private String salesOrganization;
	
	@Column(name = "region")
	private String region;
	
	@Column(name = "dp_name")
	private String dpName;
	
	@Column(name = "sales_rep_name")
	private String salesRepName;
	
	@Column(name = "product_family")
	private String productFamily;
	
	@Column(name = "product_brand")
	private String productBrand;
	
	@Column(name = "product_model")
	private String productModel;
	
	@Column(name = "bill_to_customer_number")
	private String billToCustomerNumber;
	
	@Column(name = "bill_to_account_name")
	private String billToAccountName;
	
	@Column(name = "bill_to_location_street_address")
	private String billToLocationStreetAddress;
	
	@Column(name = "bill_to_location_city")
	private String billToLocationCity;
	
	@Column(name = "bill_to_location_state")
	private String billToLocationState;
	
	@Column(name = "bill_to_location_postal_code")
	private String billToLocationPostalCode;
	
	@Column(name = "bill_to_site_category_code")
	private String billToSiteCategoryCode;
	
	@Column(name = "ship_to_customer_number")
	private String shipToCustomerNumber;
	
	@Column(name = "ship_to_customer_account_name")
	private String shipToCustomerAccountName;
	
	@Column(name = "ship_to_location_street_address")
	private String shipToLocationStreetAddress;
	
	@Column(name = "ship_to_location_city")
	private String shipToLocationCity;
	
	@Column(name = "ship_to_location_state")
	private String shipToLocationState;
	
	@Column(name = "ship_to_location_postal_code")
	private String shipToLocationPostalCode;
	
	@Column(name = "ship_to_site_category_code")
	private String shipToSiteCategoryCode;
	
	@Column(name = "end_customer_name")
	private String endCustomerName;
	
	@Column(name = "end_customer_address")
	private String endCustomerAddress;
	
	@Column(name = "end_customer_city")
	private String endCustomerCity;
	
	@Column(name = "end_customer_state")
	private String endCustomerState;
	
	@Column(name = "end_customer_short_zip")
	private String endCustomerShortZip;
	
	@Column(name = "total_actual_sales_amount")
	private String totalActualSalesAmount;
	
	@Column(name = "total_net_qty")
	private String totalNetQty;
	
	@Column(name = "total_net_sales_amt")
	private String totalNetSalesAmt;
	
	@Column(name = "total_commission_sales_amt")
	private String totalCommissionSalesAmt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getUploadOn() {
		return uploadOn;
	}

	public void setUploadOn(Date uploadOn) {
		this.uploadOn = uploadOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Date getFiscalDate() {
		return fiscalDate;
	}

	public void setFiscalDate(Date fiscalDate) {
		this.fiscalDate = fiscalDate;
	}

	public String getFiscalYearWeek() {
		return fiscalYearWeek;
	}

	public void setFiscalYearWeek(String fiscalYearWeek) {
		this.fiscalYearWeek = fiscalYearWeek;
	}

	public String getFiscalPeriod() {
		return fiscalPeriod;
	}

	public void setFiscalPeriod(String fiscalPeriod) {
		this.fiscalPeriod = fiscalPeriod;
	}

	public String getWeekOfYear() {
		return weekOfYear;
	}

	public void setWeekOfYear(String weekOfYear) {
		this.weekOfYear = weekOfYear;
	}

	public String getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getOrderedDate() {
		return orderedDate;
	}

	public void setOrderedDate(Date orderedDate) {
		this.orderedDate = orderedDate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getSalesOrganization() {
		return salesOrganization;
	}

	public void setSalesOrganization(String salesOrganization) {
		this.salesOrganization = salesOrganization;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getDpName() {
		return dpName;
	}

	public void setDpName(String dpName) {
		this.dpName = dpName;
	}

	public String getSalesRepName() {
		return salesRepName;
	}

	public void setSalesRepName(String salesRepName) {
		this.salesRepName = salesRepName;
	}

	public String getProductFamily() {
		return productFamily;
	}

	public void setProductFamily(String productFamily) {
		this.productFamily = productFamily;
	}

	public String getProductBrand() {
		return productBrand;
	}

	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}

	public String getProductModel() {
		return productModel;
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getBillToCustomerNumber() {
		return billToCustomerNumber;
	}

	public void setBillToCustomerNumber(String billToCustomerNumber) {
		this.billToCustomerNumber = billToCustomerNumber;
	}

	public String getBillToAccountName() {
		return billToAccountName;
	}

	public void setBillToAccountName(String billToAccountName) {
		this.billToAccountName = billToAccountName;
	}

	public String getBillToLocationStreetAddress() {
		return billToLocationStreetAddress;
	}

	public void setBillToLocationStreetAddress(String billToLocationStreetAddress) {
		this.billToLocationStreetAddress = billToLocationStreetAddress;
	}

	public String getBillToLocationCity() {
		return billToLocationCity;
	}

	public void setBillToLocationCity(String billToLocationCity) {
		this.billToLocationCity = billToLocationCity;
	}

	public String getBillToLocationState() {
		return billToLocationState;
	}

	public void setBillToLocationState(String billToLocationState) {
		this.billToLocationState = billToLocationState;
	}

	public String getBillToLocationPostalCode() {
		return billToLocationPostalCode;
	}

	public void setBillToLocationPostalCode(String billToLocationPostalCode) {
		this.billToLocationPostalCode = billToLocationPostalCode;
	}

	public String getBillToSiteCategoryCode() {
		return billToSiteCategoryCode;
	}

	public void setBillToSiteCategoryCode(String billToSiteCategoryCode) {
		this.billToSiteCategoryCode = billToSiteCategoryCode;
	}

	public String getShipToCustomerNumber() {
		return shipToCustomerNumber;
	}

	public void setShipToCustomerNumber(String shipToCustomerNumber) {
		this.shipToCustomerNumber = shipToCustomerNumber;
	}

	public String getShipToCustomerAccountName() {
		return shipToCustomerAccountName;
	}

	public void setShipToCustomerAccountName(String shipToCustomerAccountName) {
		this.shipToCustomerAccountName = shipToCustomerAccountName;
	}

	public String getShipToLocationStreetAddress() {
		return shipToLocationStreetAddress;
	}

	public void setShipToLocationStreetAddress(String shipToLocationStreetAddress) {
		this.shipToLocationStreetAddress = shipToLocationStreetAddress;
	}

	public String getShipToLocationCity() {
		return shipToLocationCity;
	}

	public void setShipToLocationCity(String shipToLocationCity) {
		this.shipToLocationCity = shipToLocationCity;
	}

	public String getShipToLocationState() {
		return shipToLocationState;
	}

	public void setShipToLocationState(String shipToLocationState) {
		this.shipToLocationState = shipToLocationState;
	}

	public String getShipToLocationPostalCode() {
		return shipToLocationPostalCode;
	}

	public void setShipToLocationPostalCode(String shipToLocationPostalCode) {
		this.shipToLocationPostalCode = shipToLocationPostalCode;
	}

	public String getShipToSiteCategoryCode() {
		return shipToSiteCategoryCode;
	}

	public void setShipToSiteCategoryCode(String shipToSiteCategoryCode) {
		this.shipToSiteCategoryCode = shipToSiteCategoryCode;
	}

	public String getEndCustomerName() {
		return endCustomerName;
	}

	public void setEndCustomerName(String endCustomerName) {
		this.endCustomerName = endCustomerName;
	}

	public String getEndCustomerAddress() {
		return endCustomerAddress;
	}

	public void setEndCustomerAddress(String endCustomerAddress) {
		this.endCustomerAddress = endCustomerAddress;
	}

	public String getEndCustomerCity() {
		return endCustomerCity;
	}

	public void setEndCustomerCity(String endCustomerCity) {
		this.endCustomerCity = endCustomerCity;
	}

	public String getEndCustomerState() {
		return endCustomerState;
	}

	public void setEndCustomerState(String endCustomerState) {
		this.endCustomerState = endCustomerState;
	}

	public String getEndCustomerShortZip() {
		return endCustomerShortZip;
	}

	public void setEndCustomerShortZip(String endCustomerShortZip) {
		this.endCustomerShortZip = endCustomerShortZip;
	}

	public String getTotalActualSalesAmount() {
		return totalActualSalesAmount;
	}

	public void setTotalActualSalesAmount(String totalActualSalesAmount) {
		this.totalActualSalesAmount = totalActualSalesAmount;
	}

	public String getTotalNetQty() {
		return totalNetQty;
	}

	public void setTotalNetQty(String totalNetQty) {
		this.totalNetQty = totalNetQty;
	}

	public String getTotalNetSalesAmt() {
		return totalNetSalesAmt;
	}

	public void setTotalNetSalesAmt(String totalNetSalesAmt) {
		this.totalNetSalesAmt = totalNetSalesAmt;
	}

	public String getTotalCommissionSalesAmt() {
		return totalCommissionSalesAmt;
	}

	public void setTotalCommissionSalesAmt(String totalCommissionSalesAmt) {
		this.totalCommissionSalesAmt = totalCommissionSalesAmt;
	}
}
