package com.drc.reportbuilder.entities;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cms_avaya_drc_ar")

public class CmsAvayaDrcAr implements Serializable {
	
	private static final long serialVersionUID = -2343243243242432341L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator")
	@SequenceGenerator(name="id_generator", sequenceName = "cms_avaya_drc_ar_id_seq", allocationSize=50)
	@Column(name = "id", updatable = false, nullable = false)
	private long id;
	
	@Column(name = "upload_on")
	private Date uploadOn;
	
	@Column(name = "modified_on")
	private Date modifiedOn;
	
	@Column(name = "deleted_on")
	private Date deletedOn;
	
	@Column(name = "is_active")
	private String isActive;
	
	@Column(name = "agent")
	private Integer agent;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "staffed_time")
	private Integer staffedTime;
	
	@Column(name = "aux_time")
	private Integer auxTime;
	
	@Column(name = "time_in_0")
	private Integer timeIn0;
	
	@Column(name = "time_in_1")
	private Integer timeIn1;
	
	@Column(name = "time_in_2")
	private Integer timeIn2;
	
	@Column(name = "time_in_3")
	private Integer timeIn3;
	
	@Column(name = "time_in_4")
	private Integer timeIn4;
	
	@Column(name = "time_in_5")
	private Integer timeIn5;
	
	@Column(name = "time_in_6")
	private Integer timeIn6;
	
	@Column(name = "time_in_7")
	private Integer timeIn7;
	
	@Column(name = "time_in_8")
	private Integer timeIn8;
	
	@Column(name = "time_in_9")
	private Integer timeIn9;
	
	@Column(name = "time_in_aux_10_99")
	private Integer timeInAux10_99;
	
	@Column(name = "interrupt_notifications")
	private Integer interruptNotifications;
	
	@Column(name = "accepted_interrupts")
	private Integer acceptedInterrupts;
	
	@Column(name = "rejected_interrupts")
	private Integer rejectedInterrupts;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getUploadOn() {
		return uploadOn;
	}

	public void setUploadOn(Date uploadOn) {
		this.uploadOn = uploadOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public Integer getAgent() {
		return agent;
	}

	public void setAgent(Integer agent) {
		this.agent = agent;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getStaffedTime() {
		return staffedTime;
	}

	public void setStaffedTime(Integer staffedTime) {
		this.staffedTime = staffedTime;
	}

	public Integer getAuxTime() {
		return auxTime;
	}

	public void setAuxTime(Integer auxTime) {
		this.auxTime = auxTime;
	}

	public Integer getTimeIn0() {
		return timeIn0;
	}

	public void setTimeIn0(Integer timeIn0) {
		this.timeIn0 = timeIn0;
	}

	public Integer getTimeIn1() {
		return timeIn1;
	}

	public void setTimeIn1(Integer timeIn1) {
		this.timeIn1 = timeIn1;
	}

	public Integer getTimeIn2() {
		return timeIn2;
	}

	public void setTimeIn2(Integer timeIn2) {
		this.timeIn2 = timeIn2;
	}

	public Integer getTimeIn3() {
		return timeIn3;
	}

	public void setTimeIn3(Integer timeIn3) {
		this.timeIn3 = timeIn3;
	}

	public Integer getTimeIn4() {
		return timeIn4;
	}

	public void setTimeIn4(Integer timeIn4) {
		this.timeIn4 = timeIn4;
	}

	public Integer getTimeIn5() {
		return timeIn5;
	}

	public void setTimeIn5(Integer timeIn5) {
		this.timeIn5 = timeIn5;
	}

	public Integer getTimeIn6() {
		return timeIn6;
	}

	public void setTimeIn6(Integer timeIn6) {
		this.timeIn6 = timeIn6;
	}

	public Integer getTimeIn7() {
		return timeIn7;
	}

	public void setTimeIn7(Integer timeIn7) {
		this.timeIn7 = timeIn7;
	}

	public Integer getTimeIn8() {
		return timeIn8;
	}

	public void setTimeIn8(Integer timeIn8) {
		this.timeIn8 = timeIn8;
	}

	public Integer getTimeIn9() {
		return timeIn9;
	}

	public void setTimeIn9(Integer timeIn9) {
		this.timeIn9 = timeIn9;
	}

	public Integer getTimeInAux10_99() {
		return timeInAux10_99;
	}

	public void setTimeInAux10_99(Integer timeInAux10_99) {
		this.timeInAux10_99 = timeInAux10_99;
	}

	public Integer getInterruptNotifications() {
		return interruptNotifications;
	}

	public void setInterruptNotifications(Integer interruptNotifications) {
		this.interruptNotifications = interruptNotifications;
	}

	public Integer getAcceptedInterrupts() {
		return acceptedInterrupts;
	}

	public void setAcceptedInterrupts(Integer acceptedInterrupts) {
		this.acceptedInterrupts = acceptedInterrupts;
	}

	public Integer getRejectedInterrupts() {
		return rejectedInterrupts;
	}

	public void setRejectedInterrupts(Integer rejectedInterrupts) {
		this.rejectedInterrupts = rejectedInterrupts;
	}
	
	
	
}
